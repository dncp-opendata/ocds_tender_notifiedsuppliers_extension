# Tender Notified Suppliers

This extension adds information about the suppliers that have been invited to bid in this process.

## Example

```json
{
  "parties": [
      {
        "name": "ISIS S.A.",
        "id": "fdeaf1c962e475e62efbae7e4a287c46",
        "roles": ["notifiedSupplier"]
      }
  ],
  "tender": {
    "notifiedSuppliers": [
        {
            "name": "ISIS S.A.",
            "id": "fdeaf1c962e475e62efbae7e4a287c46"
        }
    ]
  }
}
```